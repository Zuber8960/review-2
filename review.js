const data = [
    {
        "code": "TRSSD1-DR",
        "name": "Taurus Flexi Cap IDCW Reinvest Direct Plan",
        "category": "Equity",
        "reinvestment": "Y",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Multi Cap Fund",
        "plan": null,
        "returns": {
            "year_1": -1.0969,
            "year_3": -6.0201,
            "year_5": 1.3781,
            "inception": 4.9895,
            "date": "2020-09-11"
        },
        "volatility": 17.3276
    },
    {
        "code": "TRBEG1-GR",
        "name": "Taurus Bonanza Growth Direct Plan",
        "category": "Equity",
        "reinvestment": "Z",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Large Cap Fund",
        "plan": "GROWTH",
        "returns": {
            "year_1": 1.422,
            "year_3": -0.4161,
            "year_5": 4.5035,
            "inception": 7.0199,
            "date": "2020-09-11"
        },
        "volatility": 16.8901
    },
    {
        "code": "TRBED1-DP",
        "name": "Taurus Largecap Equity IDCW Payout Direct Plan",
        "category": "Equity",
        "reinvestment": "N",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Large Cap Fund",
        "plan": "DIVIDEND ANNUAL",
        "returns": {
            "year_1": 1.4316,
            "year_3": -11.8493,
            "year_5": -4.8331,
            "inception": 0.6825,
            "date": "2020-09-11"
        },
        "volatility": 21.6466
    },
    {
        "code": "TRBED1-DR",
        "name": "Taurus Largecap Equity IDCW Reinvest Direct Plan",
        "category": "Equity",
        "reinvestment": "Y",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Large Cap Fund",
        "plan": "DIVIDEND ANNUAL",
        "returns": {
            "year_1": 1.4316,
            "year_3": -11.8493,
            "year_5": -4.8331,
            "inception": 0.6825,
            "date": "2020-09-11"
        },
        "volatility": 21.6466
    },
    {
        "code": "TRDSG1-GR",
        "name": "Taurus Discovery Growth Direct Plan",
        "category": "Equity",
        "reinvestment": "Z",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Mid Cap Fund",
        "plan": "GROWTH",
        "returns": {
            "year_1": 19.8027,
            "year_3": 3.5522,
            "year_5": 9.9824,
            "inception": 13.9453,
            "date": "2020-09-11"
        },
        "volatility": 17.5363
    },
    {
        "code": "TRDSD1-DP",
        "name": "Taurus Discovery IDCW Payout Direct Plan",
        "category": "Equity",
        "reinvestment": "N",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Mid Cap Fund",
        "plan": null,
        "returns": {
            "year_1": 19.8406,
            "year_3": 3.5268,
            "year_5": 7.4212,
            "inception": 12.085,
            "date": "2020-09-11"
        },
        "volatility": 17.9407
    },
    {
        "code": "TRDSD1-DR",
        "name": "Taurus Discovery IDCW Reinvest Direct Plan",
        "category": "Equity",
        "reinvestment": "Y",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Mid Cap Fund",
        "plan": null,
        "returns": {
            "year_1": 19.8406,
            "year_3": 3.5268,
            "year_5": 7.4212,
            "inception": 12.085,
            "date": "2020-09-11"
        },
        "volatility": 17.9407
    },
    {
        "code": "TRITG1-GR",
        "name": "Taurus Infrastructure Growth Direct Plan",
        "category": "Equity",
        "reinvestment": "Z",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",

        "fund_category": "Sectoral/Thematic",
        "plan": "GROWTH",
        "returns": {
            "year_1": 5.9975,
            "year_3": -0.2779,
            "year_5": 7.9846,
            "inception": 8.9838,
            "date": "2020-09-11"
        },
        "volatility": 18.6876
    },
    {
        "code": "TRITD1-DP",
        "name": "Taurus Infrastructure IDCW Payout Direct Plan",
        "category": "Equity",
        "reinvestment": "N",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Sectoral/Thematic",
        "plan": "DIVIDEND ANNUAL",
        "returns": {
            "year_1": 5.9858,
            "year_3": -0.2841,
            "year_5": 7.794,
            "inception": 8.7802,
            "date": "2020-09-11"
        },
        "volatility": 18.7027
    },
    {
        "code": "TRITD1-DR",
        "name": "Taurus Infrastructure IDCW Reinvest Direct Plan",
        "category": "Equity",
        "reinvestment": "Y",
        "fund_house": "TAURUSMUTUALFUND_MF",
        "fund_type": "Equity",
        "fund_category": "Sectoral/Thematic",
        "plan": "DIVIDEND ANNUAL",
        "returns": {
            "year_1": 5.9858,
            "year_3": -0.2841,
            "year_5": 7.794,
            "inception": 8.7802,
            "date": "2020-09-11"
        },
        "volatility": 18.7027
    }
];




// problem 1: Find all the reinvest plans.

function findAllReinvestPlan(data) {
    const result = data.filter((element) => {
        return element.reinvestment === "Y";
    });
    return result;
}

const reinvest = findAllReinvestPlan(data);
console.log(reinvest);



// Find the average returns for each of those plans and store it inside a separate key inside returns.

function findAverageReturns(data) {
    const result = data.map(element => {
        const { returns } = element;
        const sum = Number(returns.year_1) + Number(returns.year_3) + Number(returns.year_5);
        const average = (sum / 3).toFixed(2);
        element.returns.newReturn = average;
        return element;
    });
    return result;
}


const findReturn = findAverageReturns(reinvest);
console.log(findReturn);



